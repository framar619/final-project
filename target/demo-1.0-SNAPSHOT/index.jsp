<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="CSS/style.css">
    <title>Title</title>
</head>
<body>

<div class="Container">

    <h1>Keep a record of your Body Mass Index</h1>
    <h2>Register your info in this form</h2>

</div>

<div class="image">
    <picture>
        <img src= "images/bmi.jpg" alt="banner image" />
    </picture>
</div>

<form action="Servlet2" method="post">
    <p>Name<input name="name" type="text" /> </p>
    <p>Weight<input name="weight" type="text" /> </p>
    <p>Height<input name="height" type="text" /> </p>
    <p><input type="submit" value="Save" /></p>
</form>

</body>
</html>