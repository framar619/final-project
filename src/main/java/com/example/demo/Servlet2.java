package com.example.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;


@WebServlet(name = "Servlet2", value = "/Servlet2")
public class Servlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String name = request.getParameter("name");
        String weight = request.getParameter("weight");
        String height = request.getParameter("height");
        out.println("<style>");
        out.println("table, tr, td  { border: 1px solid black}");
        out.println("</style>");
        out.println("<table>");
        //out.println("<tr>");
        out.println("<td colspan=\"2\">");
        out.println("<h1>BMI Calculator</h1>");
        out.println("</td>");
        //out.println("</tr>");
        out.println("<tr>");
        out.println("<td colspan=\"2\">");
        out.println("<strong>"+ "<p>Name: "+"</strong>"+ name + "</p>");
        out.println("</td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td colspan=\"2\">");
        out.println("<strong>"+"<p>Weight: "+"</strong>"+ weight +"</p>");
        out.println("</td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td colspan=\"2\">");
        out.println("<strong>"+"<p>Height: "+"</strong>"+ height +"</p>");
        out.println("</td>");
        out.println("</tr>");
        out.println("</table>");
        out.println("<h2>The record was saved </h2>");
        UserEntity e2 = new UserEntity();
        e2.setName(name);
        e2.setHeight(Integer.parseInt(height));
        e2.setWeight(Integer.parseInt(weight));


        try {
            if (name == ""){
                throw new Exception("Exception message");

            }
        } catch (Exception e) {
            out.println("We need a non-empty value in the name" + "</p>");
        }
        try {
            if (isNumeric(name)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We don't need a numeric value in the name"+ "</p>");
        }

        try {
            if (!isNumeric(weight)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We need a numeric value in the weight" + "</p>");
        }

        try {
            if (!isNumeric(height)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We need a numeric value in the height" + "</p>");
        }
        double result = Math.pow(Double.parseDouble(height), 2);
        DecimalFormat df = new DecimalFormat("###.##");
        out.println("<p>BMI: " + Double.parseDouble(weight) / result * 703 + "</p>"); //
        out.println("</body></html>");
        double bmiResult = Double.parseDouble(weight) / result * 703; //
        out.println(df.format(bmiResult));
        e2.setBmi(bmiResult);
        saveBMI(e2);


        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<UserEntity> user = session.createQuery("SELECT e from UserEntity e ", UserEntity.class).list();
            user.forEach(s -> {
                //dibujar tabla
                out.println("<br>");
                out.println(s.getName());
                out.println("<br>");
                out.println(s.getBmi());
            });
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        out.println("</p>"+"<a href=\"/Project_war_exploded/\">Add Record</a>");
    }
    public static boolean isNumeric (String a){
        try {
            int d = Integer.parseInt(a);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;

    }

    public static void saveBMI (UserEntity e1){



        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the employee objects
            session.save(e1);


        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        /*try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<UserEntity> user = session.createQuery("SELECT e from UserEntity e ", UserEntity.class).list();
            user.forEach(s -> {System.out.println(s.getName());System.out.println(s.getBmi());});
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }*/

    }

}
